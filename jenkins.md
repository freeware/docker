```
version: '3'
services:
  jenkins:
    image: jenkins/jenkins:lts
    container_name: jenkins-server
    ports:
      - "8080:8080"
    volumes:
      - jenkins_home:/var/jenkins_home
  ssh-agent:
    image: jenkins/ssh-agent
volumes:
  jenkins_home:
```

To run
```
docker compose up -d
```

Browser
```
http://localhost:8080/
admin / admin
```