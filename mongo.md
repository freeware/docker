```
# Working
docker run --name mongodb -p 27017:27017 -v cache_mongo_data:/data/db -d mongo

docker run --name mongo-server -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=mydbuser -e MONGO_INITDB_ROOT_PASSWORD=8i99er$sh -v cache_mongo_data:/data/db -d mongo

docker run --name mongodb -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=mydbuser -e MONGO_INITDB_ROOT_PASSWORD=mydbpass -e MONGO_INITDB_DATABASE=mydb -v cache_mongo_data:/data/db -d mongo
```

Docker Compose
```
version: '3'
services:
  mongodb:
    image: mongo
    container_name: mongodb
    ports:
      - "27017:27017"
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=passw0rd
      - MONGO_INITDB_DATABASE=studx
    volumes:
      - mongo-data:/data/db
volumes:
  mongo-data:
    driver: local
```