

### docker-compose.yml
```
version: "3.2"
services:
  rabbitmq:
    image: rabbitmq:3.13.7-management-alpine
    container_name: 'rabbitmq'
    ports:
      - 5672:5672
      - 15672:15672
    volumes:
      - rabbitmq_data:/var/lib/rabbitmq/
      - rabbitmq_log:/var/log/rabbitmq
    networks:
      - rabbitmq_network

networks:
  rabbitmq_network:
    driver: bridge

volumes:
  rabbitmq_data:
    driver: local
  rabbitmq_log:
    driver: local
```

### To access on Browser
```
url: http://localhost:15672/

username: guest
password: guest
```